import {LitElement, html} from 'lit-element';

class ButtonComponent extends LitElement {
    static get properties() {
        return {
            message: {type:Array}

        }
    }

    constructor() {
        super();
        this.navegadores = ["Chrome","Safari","Opera"];
        
    }
    render (){
        return html`
            <div>${this.message}</div>
            <div><button @click = "${this.clickHandler}">Probar</button> </div>
        `;
    }
    clickHandler(e) {
        console.log(e.target);
        this.message = "Mensaje publico"
    }
    
}
customElements.define('button-component',ButtonComponent)