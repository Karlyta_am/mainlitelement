import {LitElement, html} from 'lit-element';

class ShadowAccessComponent extends LitElement {
    static get properties() {
        return {
            message: {type:Array}

        }
    }

    constructor() {
        super();
                
    }
    render (){
        return html`
            <div id="miDiv" style="width:100px;height:100px"></div>
            <div><button @click = "${this.clickHandler}">Probar</button> </div>
        `;
    }
    clickHandler(e) {
        var miDiv = this.shadowRoot.querySelector("#miDiv");
        miDiv.style.backgroundcolor = "red"; 
    }
    
}
customElements.define('shadow-access-component',ShadowAccessComponent)