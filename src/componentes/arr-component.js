import {LitElement, html} from 'lit-element';

class ArrComponent extends LitElement {
    static get properties() {
        return {
            navegadores: {type:Array}

        }
    }

    constructor() {
        super();
        this.navegadores = ["Chrome","Safari","Opera"];
        
    }
    render (){
        return html`
            <div>
                ${this.navegadores.map(i => html `<li>${i}</li>`)}
            </div>
        `;
    }

    
}
customElements.define('arr-component',ArrComponent)