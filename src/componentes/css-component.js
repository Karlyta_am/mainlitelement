import { LitElement, html, css } from 'lit-element';

class CssComponent  extends LitElement {

  static get styles() {
    return css`
      div {
        color: red;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
        <div>Contenido estilado</div>      
    `;
  }
}

customElements.define('css-component', CssComponent);