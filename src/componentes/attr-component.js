import { LitElement, html, css } from 'lit-element';

class AttrComponent  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        areax:{attribute:true},
        areay:{attribute: true},
        miprop: {type: String}
    };
  }

  constructor() {
    super();
    this.areax = 10;
    this.areay = 20;
  }

  attributeChangedCallback(name, oldval, newval){
      console.log("Atributo modificado", name, newval);
      super.attributeChangedCallback (name, oldval, newval);
  }

  render() {
    return html`
      <p>Areax: ${this.areax}</p>
      <p>Areay: ${this.areay}</p>
      <button @click="${this.changeAtributes}">Cambiar attr desde dentro</button>
    `;
  }

  changeAtributes() {
      let rdnvla = Math.floor(Math.random()*100).toString();
      this.setAttribute('areax', rdnvla);
      this.setAttribute('areay', rdnvla);

  }
    updated(changedProperties){
        changedProperties.array.forEach ((oldValue, propName) => {
            console.log(`${propName} modificada. Valor anterior ${oldValue}`);
        });
    }

}

customElements.define('attr-component', AttrComponent);