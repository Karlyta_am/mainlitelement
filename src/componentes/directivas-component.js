import { LitElement, html, css } from 'lit-element';
import {classMap } from 'lit-html/directives/class-map';
import {styleMap} from 'lit-html/directives/style-map';

class DirectivasComponent  extends LitElement {

  static get styles() {
    return css`
      myDiv {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        classes: {type: Object};
        styles: {type:Object};
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      
    `;
  }
}

customElements.define('directivas-component', DirectivasComponent);