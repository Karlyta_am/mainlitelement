import {LitElement, html} from 'lit-element';

class SlotComponent extends LitElement {
    static get properties() {
        return {
            message: {type:Array}

        }
    }

    constructor() {
        super();
        this.message = "Mensaje";
        
    }
    render (){
        return html`
            <div>
                <p>Texto de prueba</p>
                <slot name="medio"></slot>
                <p>Texto de prueba</p>
                <slot name="final"></slot>
            </div>  
        `;
    }
    
}
customElements.define('slot-component',SlotComponent)