import { LitElement, html, css } from 'lit-element';

export class PropchComponent  extends LitElement {


  static get properties() {
    return {
        miprop: {type:Number,
                hasChanged(newVal, oldVal) {
                    if (newVal > oldVal) {
                        console.log (`${newVal} > ${oldVal}, hasChanged:true`);
                        return true;
                    } else {
                        console.log (`${newVal} <= ${oldVal}, hasChanged:false`);
                        return false;
                    }
                }
        }
    };
  }

  constructor() {
    super();
    this.miprop = 1;
  }

  updated(){
      console.log("updated");
      let mivar = new CustomEvent('event-cambio',{
          detail: {
              message: "Ha cambiado el valor"
          }
      });

      this.dispatchEvent(mivar);
  }

  getNewVal(){
      let newVal = Math.floor(Math.random()*10);
      this.miprop = newVal;
  }

  render() {
    return html`
      <p>Prop: ${this.miprop} </p>
      <button @click=" ${this.getNewVal}"> Cambiar prop</button>
    `;
  }
}

customElements.define('propch-component', PropchComponent);