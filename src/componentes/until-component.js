import {LitElement, html} from 'lit-element';
import {until } from 'lit-html/directives/until.js'

class UntilComponent extends LitElement {
    static get properties() {
        return {
            content: {type:Array}

        }
    }

    constructor() {
        super();
        this.content = fetch('imgs/content.txt').then(r=> r.text());
        
    }
    render (){
        return html`
            ${until(this.content, html `<span>Cargando...</span>`)}
        `;
    }
    
}
customElements.define('until-component',UntilComponent)